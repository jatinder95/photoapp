# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from PIL import Image
import sys, time
from django.db import models


class CategoryChoices(models.Model):
    class Meta:
        verbose_name_plural = 'categorychoices'
    cat_name = models.CharField(default="others",max_length=50)
    
    

    def __str__(self):
        return self.cat_name

class Product(models.Model):
  
    now = str(int(time.time()))
    filepath = 'gallery/'
    product_type= models.ForeignKey('CategoryChoices', on_delete=models.CASCADE,default='1')
    seo_slug = models.SlugField(max_length=100,null=True,unique=True)
    product_name = models.CharField(max_length=500,null=True)
    product_tags = models.CharField(max_length=500,null=True)
  #product_image= models.ImageField(upload_to='prod_image',blank=True)
    product_likes=models.IntegerField(default=0,blank=True, null=True)
    product_impe=models.IntegerField(default=0,blank=True, null=True)

    photo_original = models.FileField('original file upload', upload_to=filepath)
    photo_medium = models.CharField(max_length=255, blank=True)
    photo_thumb = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.product_name

    def get_thumb(self):
        return "/site_media/%s" % self.photo_thumb

    def get_medium(self):
        return "/site_media/%s" % self.photo_medium

    def get_original(self):
        return "/site_media/%s" % self.photo_original

    def save(self):
        sizes = {'thumbnail': {'height': 100, 'width': 100}, 'medium': {'height': 500, 'width': 500},}

        super(Product, self).save()
        photopath = str(self.photo_original.path)  # this returns the full system path to the original file
        print('+===' + photopath)
        im = Image.open(photopath)  # open the image using PIL
        print('this works')
	# pull a few variables out of that full path
        extension = photopath.rsplit('.', 1)[1]  # the file extension        print('this works')
        print('this works' + extension)
        filename = photopath.rsplit('/', 1)[1].rsplit('.', 1)[0]  # the file name only (minus path or extension)
        fullpath = photopath.rsplit('/', 1)[0]  # the path only (minus the filename.extension)

        # use the file extension to determine if the image is valid before proceeding
        if extension not in ['jpg', 'jpeg', 'gif', 'png']: sys.exit()

        # create medium image
        im.thumbnail((sizes['medium']['width'], sizes['medium']['height']), Image.ANTIALIAS)
        medname = filename + "_" + str(sizes['medium']['width']) + "x" + str(sizes['medium']['height']) + ".jpg"
        im.save(fullpath + '/' + medname)
        self.photo_medium = self.filepath + medname

        # create thumbnail
        im.thumbnail((sizes['thumbnail']['width'], sizes['thumbnail']['height']), Image.ANTIALIAS)
        thumbname = filename + "_" + str(sizes['thumbnail']['width']) + "x" + str(sizes['thumbnail']['height']) + ".jpg"
        im.save(fullpath + '/' + thumbname)
        self.photo_thumb = self.filepath + thumbname

        super(Product, self).save()

   # class Meta:
    #    ordering = ['-content_date']
 

    
  


  		  
  

