# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Product,CategoryChoices
from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Max
import re
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def home(request, template_name='home.html'):
	catchoice=CategoryChoices.objects.all()
	product=Product.objects.all()
	page = request.GET.get('page', 1)
	paginator = Paginator(product, 3)
	try:
		imgs = paginator.page(page)
	except PageNotAnInteger:
		imgs = paginator.page(1)
	except EmptyPage:
		imgs = paginator.page(paginator.num_pages)
	imp = Product.objects.all().aggregate(Max('product_impe'))
	qwert=str(imp)
	aa=int(re.search(r'\d+', qwert).group())
	impresult = Product.objects.filter(product_impe=aa).order_by('-id')[0]
		
	return render(request, template_name,{'product':product,'impresult':impresult,'imgs':imgs,'catchoice':catchoice})


def img(request,seo_slug, template_name='img.html'):
	catchoice=CategoryChoices.objects.all()
	product = get_object_or_404(Product,seo_slug=seo_slug)
	product_impe=product.product_impe+1
	product.product_impe=product_impe
	product.save()
	
	print product
	
		
	return render(request, template_name,{'product':product,'catchoice':catchoice})

def search(request, template_name='search.html'):
	catchoice=CategoryChoices.objects.all()
	data = request.POST.dict()
	result=Product.objects.filter( product_tags__icontains=data['search'])
	print result
	return render(request,template_name,{'result':result,'catchoice':catchoice})


def category(request,cat):
	catchoice=CategoryChoices.objects.all()
	result=Product.objects.filter(product_type__cat_name__contains=cat)
	print result

	return render(request,'category.html',{'result':result,'catchoice':catchoice})