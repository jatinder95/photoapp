# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from app.models import Product,CategoryChoices

#@admin.register(Product)
#class RateAdmin(admin.ModelAdmin):
#	fields = ('product_name', 'product_tags','product_image', 'product_likes','product_impe','product_type','seo_slug')
#	list_display = ['product_name', 'product_tags','product_image', 'product_likes','product_impe','product_type','seo_slug']
#	search_fields = ('product_name',)

class AuthorAdmin(admin.ModelAdmin):
    pass
admin.site.register(CategoryChoices, AuthorAdmin)

admin.site.register(Product)
