# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-08-30 13:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryChoices',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cat_name', models.CharField(default='others', max_length=50)),
            ],
            options={
                'verbose_name_plural': 'categorychoices',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_slug', models.SlugField(max_length=100, null=True, unique=True)),
                ('product_name', models.CharField(max_length=500, null=True)),
                ('product_tags', models.CharField(max_length=500, null=True)),
                ('product_likes', models.IntegerField(blank=True, default=0, null=True)),
                ('product_impe', models.IntegerField(blank=True, default=0, null=True)),
                ('photo_original', models.FileField(upload_to='gallery/1535634126/', verbose_name='original file upload')),
                ('photo_medium', models.CharField(blank=True, max_length=255)),
                ('photo_thumb', models.CharField(blank=True, max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('product_type', models.ForeignKey(default='1', on_delete=django.db.models.deletion.CASCADE, to='app.CategoryChoices')),
            ],
        ),
    ]
